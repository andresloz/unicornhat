#! /usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import print_function
from random import randint
import re

NAME = "color tools (objColor) python3 compatible"
VERSION = "1.3.7"
DATE = "2020 Mars 3"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "the aim of this class is to make easy a lot of color conversions and color manipulations"

class Color:
	"""
	Class Color( color=(R,G,B), alpha=A ) or Color( color=(R,G,B,A) )
	define a default color and a default alpha
	"""
	def __init__(self, color=None, alpha=255):
		color = self.getColor(color=color)
		if color:
			self.color = color
		else:
			self.color = None
			
		self.alpha = self.checkValue(val=alpha)
	
	# check colors
	def getColor(self, color=None):# check RGB, RGBA, decimal, Hexadecimal, color name, CSS Hexadecimal
		varType = type(color).__name__
		
		if varType == "NoneType" or varType == "bool":
			return None
		else:
			if varType == "int":# decimal byte
				color = (color, color, color)
			elif (varType == "tuple" or varType == "list") and 2 < len(color) < 5:
				color = tuple(color)
			elif varType == "str":	
				# string types
				matchHex = re.search(r'^#(?:[0-9a-fA-F]{3,4}){1,2}$', color) # regex for hex color ^#(?:[0-9a-fA-F]{3}){1,2}$
				matchTup = re.search("\s?\(\s?\d{1,3}\s?,\s?\d{1,3}\s?,\s?\d{1,3}\s?\)\s?", color) # match RGB tuple
				matchInt = re.search("\s?\d{1,3}\s?", color) # match 0-999 integer
				matchName = re.search(r'^[a-zA-Z]+$', color) # match color name
				
				if matchHex: # is hex color
					color = color.lower()
					if len(color) == 9: # 9 chars hex color + alpha
						color = (int(color[1:3],16), int(color[3:5],16), int(color[5:7],16), int(color[7:9],16))
					elif len(color) == 7: # 6 chars hex color
						color = (int(color[1:3],16), int(color[3:5],16), int(color[5:7],16))
					elif len(color) == 4: # 3 chars hex color (like css)
						color = (int(color[1]*2,16), int(color[2]*2,16), int(color[3]*2,16))
					else:
						return None
				elif matchTup:
					try:
						color = eval(color)
					except:
						return None
				elif matchInt:
					try: # ex "127" instead of 127 decimal string but Hexadecimal color
						color = tuple(map(int, (color, color, color)))
					except: # if there is no decimal return None
						return None
				elif matchName:
					try:
						color = self.getRGBbyName(color)
					except: # if there is no decimal return None
						return None
				else:
					return None
			else: # there is no color value
				return None

		color = self.checkArray(color=color) # check for float or > 255 > 0
		# return RGB or RGBA tuple
		return color
		
	def checkArray(self, color=None):
		result = []
		for c in color:
			c = self.checkValue(c)
			result.append(c)
			
		return tuple(result)
		
	def checkValue(self, val=None):
		try:
			val = min(255, max(0, int(val)))
		except:
			val = 255 # return full byte
			
		return val
	
	# ************* set colors
	def setRGB(self, color=None):
		if color:
			self.color = self.getRGB(color=color)
		else:
			return False
			
		return True
		
	def setRGBA(self, color=None, alpha=None):
		if color and alpha:
			self.color = self.getRGB(color=color)
			self.alpha = self.checkValue(val=alpha)
		elif color:
			color = self.getColor(color=color)
			if color and len(color) == 4: # RGBA color
				self.color = color[0:3]
				self.alpha = color[3]
			elif color and len(color) == 3: # RGB color
				self.color = self.getRGB(color=color)
				# self.alpha remains
		elif alpha:
			self.alpha = self.checkValue(val=alpha)
		else: # no values
			return False
			
		return True
		
	def setA(self, alpha=255):
		if alpha != 255:
			alpha = self.checkValue(val=alpha)
			
		self.alpha = alpha     
		return True
		
	# ************* functions to return colors values
	def getRGB(self, color=None):
		if color == None: # color value could be 0 (zero)
			color = self.color
			
		color = self.getColor(color=color)
		
		if color:
			if len(color) == 4: # if RGBA color
				return color[0:3]
			else:
				return color
		else:
			return self.getRandomRGB()
			
	def getRGBA(self, color=None, alpha=None):
		if color and alpha: 
			colorArray = list(self.getRGB(color=color)) # take only RGB from color
			alpha = self.checkValue(val=alpha)
			colorArray.append(alpha)
			return tuple(colorArray)
		elif color:
			colorArray = list(self.getRGB(color=color))
			alpha = self.getA(color=color) # try to take alpha from color or keep self.alpha
			colorArray.append(alpha)
			return tuple(colorArray)
		elif alpha: # if only alpha and not RGB
			colorArray = list(self.getRGB()) # take random RGB
			alpha = self.checkValue(val=alpha) # take alpha or default 255
			colorArray.append(alpha)
			return tuple(colorArray)
		else: # not color and not alpha
			colorArray = list(self.getRGB()) # take random RGB
			colorArray.append(self.alpha)
			return tuple(colorArray)
		
	def getR(self, color=None):
		color = self.getRGB(color=color)
		return color[0]
		
	def getG(self, color=None):
		color = self.getRGB(color=color)
		return color[1]
		
	def getB(self, color=None):
		color = self.getRGB(color=color)
		return color[2]
		
	def getA(self, color=None):
		if color:
			color = self.getColor(color=color)
			if color and len(color) == 4: # RGBA color
				return color[3]
			else:
				return self.alpha
		else:
			return self.alpha
		
	def getRandomRGB(self):
		color = (randint(0,255), randint(0,255), randint(0,255))
		return color
		
	def getRandomRGBA(self, alpha=None):
		if alpha:
			alpha = self.checkValue(alpha)
		else:
			alpha = self.alpha
			
		color = (randint(0,255), randint(0,255), randint(0,255), alpha)
		return color
		
	def getHexRGB(self, color=None):
		color = self.getRGB(color=color)
		colorHex = '#{:02x}{:02x}{:02x}'.format(*color)
		return colorHex
		
	def getHexRGBA(self, color=None):
		color = self.getRGBA(color=color)
		colorHex = '#{:02x}{:02x}{:02x}{:02x}'.format(*color)
		return colorHex
		
	def getCssRGB(self, color=None):
		color = self.getRGB(color=color)
		colorCss = "rgb"+repr(color)
		return colorCss
		
	def getCssRGBA(self, color=None):
		color = list(self.getRGB(color=color))
		color.append(self.getRGBAscaleColor(color=color)[-1])
		colorCss = "rgba"+repr(tuple(color))
		return colorCss
	
	def getInv(self, color=None): # RGB and RGBA
		if color == None:
			color = self.color
			
		color = self.getColor(color=color)
		
		if color and len(color) == 4:
			color = self.getRGBA(color=color)
		elif color:
			color = self.getRGB(color=color)
		else:
			return False
			
		color = [255 - x for x in color]
		return tuple(color)
			
	def getInvHex(self, color=None):
		if color == None:
			color = self.color
			
		color = self.getInv(color=color)
		
		if color and len(color) == 4:
			colorHex = self.getHexRGBA(color=color)
		elif color:
			colorHex = self.getHexRGB(color=color)
		else:
			return False
			
		return colorHex
	
	# ************* functions to return others values
	def getRGBbytes(self, color=None):
		color = self.getRGB(color=color)
		bytes = b''.join(map(chr,color))
		return bytes
	
	def getRGBAbytes(self, color=None):
		color = self.getRGBA(color=color)
		bytes = b''.join(map(chr,color))
		return bytes	
			
	def getRGBscaleColor(self, color=None):
		color = self.getRGB(color=color)
		# convert to float factor 0 (0.0) -> 1.0 (255)
		color = tuple([x / 255.0 for x in color])
		return color
		
	def getRGBAscaleColor(self, color=None):
		color = self.getRGBA(color=color)
		# convert to float factor 0 (0.0) -> 1.0 (255)
		color = tuple([x / 255.0 for x in color])
		return color
	
	def getMinMaxRGB(self, color=None):
		color = self.getRGB(color=color)
		colorMin = []
		colorMax = []
		for value in color:
			maxMinDeviation = value / 2
			remainder = 255 - value
			if value > 0:
				if maxMinDeviation > remainder:
					maxMinDeviation = remainder
				min = value - maxMinDeviation
				max = value + maxMinDeviation
				colorMin.append(min)
				colorMax.append(max)
			else:
				colorMin.append(0)
				colorMax.append(0)
				
		return tuple(colorMin),tuple(colorMax)
		
	def getName(self, color=None):
		colormap = self.getColorNames()
		color = self.getHexRGB(color=color)
		for name, hex in colormap.items():
			if hex == color:
				return name
		
		return None
		
	def getHexByName(self, name=None):
		colormap = self.getColorNames()
		for colorName, hex in colormap.items():
			if colorName == name:
				return hex
		
		return None
		
	def getRGBbyName(self, name=None):
		colormap = self.getColorNames()
		for colorName, hex in colormap.items():
			if colorName == name:
				return self.getRGB(color=hex)
		
		return None
		
	def getRGBAbyName(self, name=None):
		colormap = self.getColorNames()
		for colorName, hex in colormap.items():
			if colorName == name:
				return self.getRGBA(color=hex)
		
		return None
		
	def getBetweenRGB(self, a=(0,0,0), b=(255,255,255)):
		a = self.getRGB(color=a)
		b = self.getRGB(color=b)
		color = []
		for i in range(0,3):
			color.append(self.getRandBetween(a[i],b[i]))
			
		result = tuple(color)
		return result
		
	def getBetweenRGBA(self, a=(0,0,0,0), b=(255,255,255,255)):
		a = self.getRGBA(color=a)
		b = self.getRGBA(color=b)
		color = []
		for i in range(0,4):
			color.append(self.getRandBetween(a[i],b[i]))
			
		result = tuple(color)
		return result
		
	def getRandBetween(self, a=0, b=0):
		if a > b:
			return randint(b, a)
		elif a < b:
			return randint(a, b)
		else:
			return a
			
	def getCMYK(self, color=None, scale=100):
		r,g,b = self.getRGB(color)
		cmyk_scale = scale
		if (r == 0) and (g == 0) and (b == 0):
			# black
			return (0, 0, 0, cmyk_scale)

		# rgb [0,255] -> cmy [0,1]
		c = 1 - r / 255.
		m = 1 - g / 255.
		y = 1 - b / 255.

		# extract out k [0,1]
		min_cmy = min(c, m, y)
		c = (c - min_cmy) / (1 - min_cmy)
		m = (m - min_cmy) / (1 - min_cmy)
		y = (y - min_cmy) / (1 - min_cmy)
		k = min_cmy

		# rescale to the range [0,cmyk_scale]
		return (c*cmyk_scale, m*cmyk_scale, y*cmyk_scale, k*cmyk_scale)
		
	def getLuminosity(self, color=None):
		# luminosity method 0.21 R + 0.72 G + 0.07 B
		r,g,b = self.getRGB(color)
		luminosity = sum([r*0.21, g*0.72, b*0.07])
		return int(luminosity)
		
	def extendPalette(self,palette=[]):
		diff = 256 - (len(palette)/3)
		extension = [0,0,0] * diff
		palette.extend(extension)
		return tuple(palette)
			
	def getPaletteVga(self, short=0):
		palette = [0,0,0,0,0,170,0,170,0,0,170,170,170,0,0,170,0,170,170,85,0,170,170,170,85,85,85,85,85,255,85,255,85,85,255,255,255,85,85,255,85,255,255,255,85,255,255,255,0,0,0,20,20,20,32,32,32,44,44,44,56,56,56,69,69,69,81,81,81,97,97,97,113,113,113,130,130,130,146,146,146,162,162,162,182,182,182,203,203,203,227,227,227,255,255,255,0,0,255,65,0,255,125,0,255,190,0,255,255,0,255,255,0,190,255,0,125,255,0,65,255,0,0,255,65,0,255,125,0,255,190,0,255,255,0,190,255,0,125,255,0,65,255,0,0,255,0,0,255,65,0,255,125,0,255,190,0,255,255,0,190,255,0,125,255,0,65,255,125,125,255,158,125,255,190,125,255,223,125,255,255,125,255,255,125,223,255,125,190,255,125,158,255,125,125,255,158,125,255,190,125,255,223,125,255,255,125,223,255,125,190,255,125,158,255,125,125,255,125,125,255,158,125,255,190,125,255,223,125,255,255,125,223,255,125,190,255,125,158,255,182,182,255,199,182,255,219,182,255,235,182,255,255,182,255,255,182,235,255,182,219,255,182,199,255,182,182,255,199,182,255,219,182,255,235,182,255,255,182,235,255,182,219,255,182,199,255,182,182,223,182,182,255,199,182,255,219,182,255,235,182,255,255,182,235,255,182,219,255,182,199,255,0,0,113,28,0,113,56,0,113,85,0,113,113,0,113,113,0,85,113,0,56,113,0,28,113,0,0,113,28,0,113,56,0,113,85,0,113,113,0,85,113,0,56,113,0,28,113,0,0,113,0,0,113,28,0,113,56,0,113,85,0,113,113,0,85,113,0,56,113,0,28,113,56,56,113,69,56,113,85,56,113,97,56,113,113,56,113,113,56,97,113,56,85,113,56,69,113,56,56,113,69,56,113,85,56,113,97,56,113,113,56,97,113,56,85,113,56,69,113,56,56,113,56,56,113,69,56,113,85,56,113,97,56,113,113,56,97,113,56,85,113,56,69,113,81,81,113,89,81,113,97,81,113,105,81,113,113,81,113,113,81,105,113,81,97,113,81,89,113,81,81,113,89,81,113,97,81,113,105,81,113,113,81,105,113,81,97,113,81,89,113,81,81,113,81,81,113,89,81,113,97,81,113,105,81,113,113,81,105,113,81,97,113,81,89,113,0,0,65,16,0,65,32,0,65,48,0,65,65,0,65,65,0,48,65,0,32,65,0,16,65,0,0,65,16,0,65,32,0,65,48,0,65,65,0,48,65,0,32,65,0,16,65,0,0,65,0,0,65,16,0,65,32,0,65,48,0,65,65,0,48,65,0,32,65,0,16,65,32,32,65,40,32,65,48,32,65,56,32,65,65,32,65,65,32,56,65,32,48,65,32,40,65,32,32,65,40,32,65,48,32,65,56,32,65,65,32,56,65,32,48,65,32,40,65,32,32,65,32,32,65,40,32,65,48,32,65,56,32,65,65,32,56,65,32,48,65,32,40,65,44,44,65,48,44,65,52,44,65,60,44,65,65,44,65,65,44,60,65,44,52,65,44,48,65,44,44,65,48,44,65,52,44,65,60,44,65,65,44,60,65,44,52,65,44,48,65,44,44,65,44,44,65,48,44,65,52,44,65,60,44,65,65,44,60,65,44,52,65,44,48,65]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
		
	def getPaletteWeb(self, short=0):
		palette = [255,192,203,255,182,193,255,105,180,255,20,147,219,112,147,199,21,133,255,160,122,250,128,114,233,150,122,240,128,128,205,92,92,220,20,60,178,34,34,139,0,0,255,0,0,255,69,0,255,99,71,255,127,80,255,140,0,255,165,0,255,255,0,255,255,224,255,250,205,250,250,210,255,239,213,255,228,181,255,218,185,238,232,170,240,230,140,189,183,107,255,215,0,255,248,220,255,235,205,255,228,196,255,222,173,245,222,179,222,184,135,210,180,140,188,143,143,244,164,96,218,165,32,184,134,11,205,133,63,210,105,30,139,69,19,160,82,45,165,42,42,128,0,0,85,107,47,128,128,0,107,142,35,154,205,50,50,205,50,0,255,0,124,252,0,127,255,0,173,255,47,0,255,127,0,250,154,144,238,144,152,251,152,143,188,143,60,179,113,46,139,87,34,139,34,0,128,0,0,100,0,102,205,170,0,255,255,0,255,255,224,255,255,175,238,238,127,255,212,64,224,208,72,209,204,0,206,209,32,178,170,95,158,160,0,139,139,0,128,128,176,196,222,176,224,230,173,216,230,135,206,235,135,206,250,0,191,255,30,144,255,100,149,237,70,130,180,65,105,225,0,0,255,0,0,205,0,0,139,0,0,128,25,25,112,230,230,250,216,191,216,221,160,221,238,130,238,218,112,214,255,0,255,255,0,255,186,85,211,147,112,219,138,43,226,148,0,211,153,50,204,139,0,139,128,0,128,75,0,130,72,61,139,102,51,153,106,90,205,123,104,238,255,255,255,255,250,250,240,255,240,245,255,250,240,255,255,240,248,255,248,248,255,245,245,245,255,245,238,245,245,220,253,245,230,255,250,240,255,255,240,250,235,215,250,240,230,255,240,245,255,228,225,220,220,220,211,211,211,192,192,192,169,169,169,128,128,128,105,105,105,119,136,153,112,128,144,47,79,79]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
		
	def getPalette32(self, short=0):
		palette = [55,255,240,245,245,220,245,222,179,210,180,140,195,176,145,192,192,192,128,128,128,70,70,70,0,0,128,8,76,158,0,0,205,0,127,255,0,255,255,127,255,212,0,128,128,34,139,34,128,128,0,127,255,0,191,255,0,255,215,0,218,165,32,255,127,80,250,128,114,252,15,192,255,119,255,204,136,153,224,176,255,181,126,220,132,49,121,75,0,130,128,0,0,220,20,60]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
		
	def getPalette16(self, short=0):
		palette = [0,0,0,255,255,255,255,0,0,0,255,0,0,0,255,255,255,0,0,255,255,255,0,255,192,192,192,128,128,128,128,0,0,128,128,0,0,128,0,128,0,128,0,128,128,0,0,128]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
		
	def getPalette8(self, short=0):
		palette = [255,0,0,255,255,0,255,255,255,255,0,255,0,0,255,0,0,0,0,255,0,0,255,255]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
	
	def getPaletteApple2GR(self, short=0):
		palette = [255,153,204,153,153,153,51,204,0,102,102,0,204,51,255,102,255,153,204,204,204,204,204,51,153,153,255,0,153,255,0,0,0,255,102,0,51,51,204,255,255,255,0,102,51,153,0,102]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
	
	def getPaletteApple2HGR(self, short=0):
		palette = [0,153,255,51,204,0,204,51,255,255,102,0,0,0,0,255,255,255]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
		
	def getPaletteHama(self, short=0):
		palette = [255,255,255,255,255,128,247,187,9,237,80,3,213,0,0,255,128,128,128,0,160,0,0,160,55,55,255,0,128,64,5,224,131,128,64,64,128,128,128,0,0,0,157,79,0,210,150,38,176,0,88,253,193,193,251,201,151,0,81,0,111,55,82,147,147,255,252,101,101,190,125,255,174,174,255,194,253,87,254,169,245]
		
		if not short:
			palette = self.extendPalette(palette)
			
		return palette
			
	def getColorNames(self):
		colormap = {
			# X11 colour table from https://drafts.csswg.org/css-color-4/ with
			# gray/grey spelling issues fixed.  This is a superset of HTML 4.0
			# colour names used in CSS 1.
			"aliceblue": "#f0f8ff","antiquewhite": "#faebd7","aqua": "#00ffff","aquamarine": "#7fffd4","azure": "#f0ffff","beige": "#f5f5dc","bisque": "#ffe4c4","black": "#000000","blanchedalmond": "#ffebcd","blue": "#0000ff","blueviolet": "#8a2be2","brown": "#a52a2a","burlywood": "#deb887","cadetblue": "#5f9ea0","chartreuse": "#7fff00","chocolate": "#d2691e","coral": "#ff7f50","cornflowerblue": "#6495ed","cornsilk": "#fff8dc","crimson": "#dc143c","cyan": "#00ffff","darkblue": "#00008b","darkcyan": "#008b8b","darkgoldenrod": "#b8860b","darkgray": "#a9a9a9","darkgrey": "#a9a9a9","darkgreen": "#006400","darkkhaki": "#bdb76b","darkmagenta": "#8b008b","darkolivegreen": "#556b2f","darkorange": "#ff8c00","darkorchid": "#9932cc","darkred": "#8b0000","darksalmon": "#e9967a","darkseagreen": "#8fbc8f","darkslateblue": "#483d8b","darkslategray": "#2f4f4f","darkslategrey": "#2f4f4f","darkturquoise": "#00ced1","darkviolet": "#9400d3","deeppink": "#ff1493","deepskyblue": "#00bfff","dimgray": "#696969","dimgrey": "#696969","dodgerblue": "#1e90ff","firebrick": "#b22222","floralwhite": "#fffaf0","forestgreen": "#228b22","fuchsia": "#ff00ff","gainsboro": "#dcdcdc","ghostwhite": "#f8f8ff","gold": "#ffd700","goldenrod": "#daa520","gray": "#808080","grey": "#808080","green": "#008000","greenyellow": "#adff2f","honeydew": "#f0fff0","hotpink": "#ff69b4","indianred": "#cd5c5c","indigo": "#4b0082","ivory": "#fffff0","khaki": "#f0e68c","lavender": "#e6e6fa","lavenderblush": "#fff0f5","lawngreen": "#7cfc00","lemonchiffon": "#fffacd","lightblue": "#add8e6","lightcoral": "#f08080","lightcyan": "#e0ffff","lightgoldenrodyellow": "#fafad2","lightgreen": "#90ee90","lightgray": "#d3d3d3","lightgrey": "#d3d3d3","lightpink": "#ffb6c1","lightsalmon": "#ffa07a","lightseagreen": "#20b2aa","lightskyblue": "#87cefa","lightslategray": "#778899","lightslategrey": "#778899","lightsteelblue": "#b0c4de","lightyellow": "#ffffe0","lime": "#00ff00","limegreen": "#32cd32","linen": "#faf0e6","magenta": "#ff00ff","maroon": "#800000","mediumaquamarine": "#66cdaa","mediumblue": "#0000cd","mediumorchid": "#ba55d3","mediumpurple": "#9370db","mediumseagreen": "#3cb371","mediumslateblue": "#7b68ee","mediumspringgreen": "#00fa9a","mediumturquoise": "#48d1cc","mediumvioletred": "#c71585","midnightblue": "#191970","mintcream": "#f5fffa","mistyrose": "#ffe4e1","moccasin": "#ffe4b5","navajowhite": "#ffdead","navy": "#000080","oldlace": "#fdf5e6","olive": "#808000","olivedrab": "#6b8e23","orange": "#ffa500","orangered": "#ff4500","orchid": "#da70d6","palegoldenrod": "#eee8aa","palegreen": "#98fb98","paleturquoise": "#afeeee","palevioletred": "#db7093","papayawhip": "#ffefd5","peachpuff": "#ffdab9","peru": "#cd853f","pink": "#ffc0cb","plum": "#dda0dd","powderblue": "#b0e0e6","purple": "#800080","rebeccapurple": "#663399","red": "#ff0000","rosybrown": "#bc8f8f","royalblue": "#4169e1","saddlebrown": "#8b4513","salmon": "#fa8072","sandybrown": "#f4a460","seagreen": "#2e8b57","seashell": "#fff5ee","sienna": "#a0522d","silver": "#c0c0c0","skyblue": "#87ceeb","slateblue": "#6a5acd","slategray": "#708090","slategrey": "#708090","snow": "#fffafa","springgreen": "#00ff7f","steelblue": "#4682b4","tan": "#d2b48c","teal": "#008080","thistle": "#d8bfd8","tomato": "#ff6347","turquoise": "#40e0d0","violet": "#ee82ee","wheat": "#f5deb3","white": "#ffffff","whitesmoke": "#f5f5f5","yellow": "#ffff00","yellowgreen": "#9acd32",
		}
		return colormap		
	
	
if __name__ == "__main__":
	print(NAME,VERSION)
	print("\n".join([DATE,AUTHOR,COPYRIGHT,URL]))
	print(DESCRIPTION)
	print()
	print("class Color()")
	print()
	o = Color()
	print("o = Color()","... no argument => random color")
	print("o.getRGB()",o.getRGB(),"...random color")
	print("o.getRGBA()",o.getRGBA(),"...random color and only alpha 255")
	del o
	print()
	o = Color((22,55,66))
	print("o = Color((22,55,66))","...no alpha => alpha 255")
	print("o.getRGB()",o.getRGB(),"...color")
	print("o.getRGBA()",o.getRGBA(),"...color and alpha 255")
	del o
	print()
	o = Color((10,100,255),100)
	print("o = Color((10,100,255),100)","...set color and alpha 100")
	print("o.getRGB()",o.getRGB(),"...color")
	print("o.getRGBA()",o.getRGBA(),"...color and alpha 100")
	print("o.getRGBA(\"aliceblue\",50)",o.getRGBA("aliceblue",50),"...color and alpha 50")
	print("o.getRGBA((10,100,255),25)",o.getRGBA((10,100,255),25),"...color and alpha 25")
	print("o.getRGBA((10,100,255,10))",o.getRGBA((10,100,255,10)),"...color and alpha 10")
	print("o.getRGBA(\"#ff00ff14\")",o.getRGBA("#ff00ff14"),"...color alpha 20")
	print("o.getRGB((10,100,255,10))",o.getRGB((10,100,255,10)),"...color")
	print()
	o = Color(alpha=100)
	print("o = Color(alpha=100)","...set only alpha declared")
	print("o.getRGB()",o.getRGB(),"...color")
	print("o.getRGBA()",o.getRGBA(),"...color and alpha")
	print()
	# o = Color("#ff00ff14")
	# print("o = Color(\"#ff00ff14\")"
	# print("o.getRGB()",o.getRGB(),"...random color" 
	# print("o.getRGBA()",o.getRGBA(),"...random color and alpha 20"
	# print
	o = Color()
	print("o = Color()","... no argument","setting method")
	print("o.setRGB((90,127,55))","return",o.setRGB((90,127,55)))
	print("o.getA()","return",o.getA())
	print("o.setRGBA((90,127,55))","return",o.setRGBA((90,127,55)))
	print("o.getA()","return",o.getA())
	print("o.setRGBA((90,127,55,75))","return",o.setRGBA((90,127,55,75)))
	print("o.getA()","return",o.getA())
	print("o.setRGBA((90,127,55),80)","return",o.setRGBA((90,127,55),80))
	print("o.getA()","return",o.getA())
	print("o.setA(252)","return",o.setA(252))
	print("o.getR()","return",o.getR())
	print("o.getG()","return",o.getG())
	print("o.getB()","return",o.getB())
	print("o.getA()","return",o.getA())
	print()
	o = Color((55,33,99),70)
	print("o = Color((55,33,99),70)")
	print("o.getRGB(\"#f08\")","return",o.getRGB("#f08"))
	print("o.getInv(\"#ff33ff\")","return",o.getInv("#ff33ff"))
	print("o.getInv(250)","return",o.getInv(250))
	print("o.getInv()","return",o.getInv())
	print("o.getInvHex()","return",o.getInvHex())
	print("o.getHexRGB(\"aliceblue\")","return",o.getHexRGB("aliceblue"))
	print("o.getHexRGB(100)","return",o.getHexRGB(100))
	print("o.getHexRGBA(100)","return",o.getHexRGBA(100))
	print("o.getRGB(127)","return",o.getRGB(127))
	print("o.getRGB(\"#0000ff\")","return",o.getRGB("#0000ff"))
	print("o.getRGBA()","return",o.getRGBA())
	print("o.getRGBscaleColor(\"#8833ff\")","return",o.getRGBscaleColor("#8833ff"))
	print("o.getRGBAscaleColor(\"#8833ff\")","return",o.getRGBAscaleColor("#8833ff"))
	print("o.getName(\"#f0f8ff\")","return",o.getName("#f0f8ff"))
	print("o.getName((240,248,255))","return",o.getName((240,248,255)))
	print("o.getName(\"#fff2e3\") no color name","return",o.getName("#fff2e3"))
	print("o.getHexByName(\"aliceblue\")","return",o.getHexByName("aliceblue"))
	print("o.getRGBbyName(\"aliceblue\")","return",o.getRGBbyName("aliceblue"))
	print()
	o = Color("aliceblue",50)
	print("o = Color(\"aliceblue\",50)")
	print("o.getRGB()","return",o.getRGB())
	print("o.getRGBA()","return",o.getRGBA())
	print("o.getCssRGB()","return",o.getCssRGB())
	print("o.getCssRGB(\"red\")","return",o.getCssRGB("red"))
	print("o.getCssRGBA()","return",o.getCssRGBA())
	print()
	a = o.getRGB(127)
	b = o.getRGB("#ffffff")
	print("a",a)
	print("b",b)
	for x in range(3):
		print("o.getBetweenRGB(a, b)","return",o.getBetweenRGB(a, b))
	print
	a = o.getRGBA((100,100,100,30))
	b = o.getRGBA((200,200,200))
	print("a",a)
	print("b",b)
	for x in range(3):
		print("o.getBetweenRGBA(a, b)","return",o.getBetweenRGBA(a, b))
		
	print()
	print("o.getMinMaxRGB(\"#8833ff\")","return",o.getMinMaxRGB("#8833ff"))
	print()
	
	print("o.getLuminosity(\"black\")","return",o.getLuminosity("black"))
	print("o.getLuminosity(\"blue\")","return",o.getLuminosity("blue"))
	print("o.getLuminosity(\"green\")","return",o.getLuminosity("green"))
	print("o.getLuminosity(\"yellow\")","return",o.getLuminosity("yellow"))
	print("o.getLuminosity(\"white\")","return",o.getLuminosity("white"))
	print()
	del o
		
