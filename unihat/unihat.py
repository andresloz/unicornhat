#! /usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function, division
from time import sleep
from .ascii_8x8_pixel import AsciiPixels
from .colors import Color

NAME = "Image Unicorn Hat"
VERSION = "0.0.1"
DATE = "2019 Dec 21"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "pimodori unicorn hat"

"""
Actual rotation will be snapped to the nearest 90 degrees.
Brightness from 0.0 to 1.0 (default 0.5)
xy coordinates of start drawing
10-100 for scrolling speed
color rgb for text
"""

class UniHat:
	def __init__(self, params={"text":"UNICORN HAT HD CLASS", "times":3, "rotation":0, "brightness":0.6, "xy":(0,0), "speed":70, "color":(127,127,127)}):
		for k,v in params.items():
			setattr(self,k,v)

		try:
			import unicornhathd as u
			self.u = u
			self.size = u.get_shape()
		except:
			self.u = None
			self.size = (16,16)
			print("need to install unicornhat library!")

	def show(self, time=None):
		self.u.show()
		if time:
			sleep(time)

	def clear(self):
		self.u.clear()

	def off(self):
		self.u.off()

	def pixelsToData(self, pixels=None):
		if len(pixels) >= (self.size[0] * self.size[1]):
			data = []
			i = 0
			for y in range(self.size[1]):
				for x in range(self.size[0]):
					r,g,b = pixels[i]
					if r or g or b:
						data.append((y,x,r,g,b))
					i += 1

			return data
		else:
			return 0

	def openImage(self, file=None):
		try:
			from PIL import Image
		except:
			print("need to install PIL library")
			return

		image = Image.open(file)
		image = image.resize(self.size)
		pixels = list(image.getdata())
		data = self.pixelsToData(pixels)
		return data

	def openData(self, fileName=None):
		try:
			with open(fileName) as f:
				data = eval(f.read())

			return data
		except:
			return 0

	def putData(self, data=[], all=0):
		if self.u:
			for y,x,r,g,b in data:
				if r or g or b or all:
					self.u.set_pixel(y,x,r,g,b)

	# put rgb list 
	def putRgbList(self, rgblist=[], xy=(0,0), wh=(1,1), all=0):
		w, h = wh
		x, y = xy
		if self.u and len(rgblist) == (w * h):
			index = 0
			for j in range(y,h):
				for i in range(x,w):
					r,g,b = rgblist[index]
					if r or g or b or all:
						self.u.set_pixel(j,i,r,g,b)
					index += 1

	def scrollText(self, text=None, xy=None, color=None, speed=None):
		if self.u:
			if text:
				spc = " " * (self.size[0] // 8)
				text = spc+text # put space at start
				self.u.rotation(self.rotation)
				self.u.brightness(self.brightness)
				if xy:
					x,y = xy
				else:
					x,y = self.xy

				if not color:color = self.color
				if not speed:speed = self.speed

				for i,char in enumerate(text):
					for col in range(8):
						self.u.clear()
						self.putData(self.getChar(char=char,xy=(x-col,y),color=color))
						for j in range(1,(self.size[0]//8)+1): # next char if exist
							try:
								self.putData(self.getChar(char=text[i+j],xy=((x-col)+(8*j),y),color=color))
							except:
								pass

						self.u.show()
						sleep(1.0 * ((100 - speed) // 1000.0))

				self.u.clear()
				return 1 
			else:
				return 0

	def scrollScreen(self, direction="", times=1, speed=None):
		if self.u:
			if not speed:speed = self.speed
			if not times:times = self.times
			_list = list(range(self.size[0]))
			if direction == "left" or direction == "l":
				_list = _list[1:]
				_list.append(0)
			else:
				last = _list.pop()
				_list.insert(0, last)

			loops = times * self.size[0]
			for i in range(loops):
				data = self.u.get_pixels()
				self.u._buf = data[:, _list]
				self.u.show()
				sleep(1.0 * ((100 - speed) // 1000.0))

	def getChar(self, char=None, xy=None, color=None):
		data = []
		charArray = AsciiPixels().getCharData(char)
		r,g,b = Color().getRGB(color)
		for y in range(8):
			for x in range(8):
				if 0 <= xy[0]+x < self.size[0] and 0 <= xy[1]+y < self.size[1] and charArray[y][x] == 1:
					data.append([xy[1]+y,xy[0]+x ,r,g,b])

		return data

if __name__ == "__main__":
	print(NAME,VERSION)
	print("\n".join([DATE,AUTHOR,COPYRIGHT,URL]))
	u = UniHat()
	u.clear()
	u.scrollText(text=NAME)
	u.off()

