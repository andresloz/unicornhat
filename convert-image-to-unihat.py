#! /usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import print_function
from unihat import UniHat
import sys, os
try:
	from PIL import Image
except:
	print("need to install PIL library")

NAME = "Unicorn hat hd screen convert image"
VERSION = "0.0.1"
DATE = "2020 January 2"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "Convert any image to 16x16 unicornhat hd \nexample: python convert-image-to-unihat.py filename"

if __name__ == "__main__":
	print(NAME)
	if len(sys.argv) > 1:
		file = sys.argv[1]
		base, ext = os.path.splitext(file)
	else:
		print(DESCRIPTION)
		x = raw_input()
		exit()
	
	u = UniHat()
	data = u.openImage(file)
	datafile = base+".dat"
	with open(datafile,"w") as f:
		f.write(repr(data))
		
	print(datafile,'created !')
	
	# ~ if unicornhat on
	# ~ u.putData(data)
	# ~ u.show()
	
	# ~ print('show image !')	
