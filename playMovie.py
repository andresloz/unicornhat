#! /usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import print_function
from os import listdir, path, system
import sys, time, tarfile, RPi.GPIO as GPIO

from unihat import UniHat

try:
	import pygame
	audioK = 1
except:
	audioK = 0

NAME = "Show Unicorhat Movie"
VERSION = "0.0.1"
DATE = "2021 August 15"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"
DESCRIPTION = "show text, read data from archive of pictures files and send them to unicorn hat display 16x16 led"

"""
example with raspberry pi
in order to use this script you have to install spi and unicornhat
if your rapsberry pi have sound output you must to install pygame if you want sound

this script is dedicated for videos done with "Movie Pattern" module from "PMT (pattern maker tools)" by Loz from http://andre-lozano.org
put directories which contains tar archive, sound and frameset in "videos" directory
put videos and unihat directories in the same directory as this file
add "python /home/pi/playMovie.py &" in /etc/rc.local if you want autostart
"""

def getDirectories(dir=".", exclude=[]):
	directories = []
	for file in sorted(listdir(dir)):
		if path.isdir(path.join(dir, file)):
			if not file in exclude:
				directories.append(path.join(dir, file))

	return directories

def loadMusic(file=None):
	if audioK:
		try:
			pygame.mixer.music.load(file)
		except:
			pass

def playMusic(file=None):
	if audioK:
		try:
			pygame.mixer.music.play()
		except:
			pass

def stopMusic():
	if audioK:
		try:
			pygame.mixer.music.stop()
		except:
			pass

if __name__ == "__main__":
	GPIO.setmode(GPIO.BCM)

	# button is pin -> button -> ground
	nextDirButton = 26
	exitButton = 19

	if nextDirButton:GPIO.setup(nextDirButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # next directory button
	if exitButton:GPIO.setup(exitButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # exit from show button

	u = UniHat()
	u.clear()
	u.show()
	u.scrollText(text="Led Player by Loz", xy=(0,4), color="red", speed=20)

	#  movies parameters
	d = 0 # dirs in use (videos)
	frames = [] # frames

	if audioK:pygame.init()
	videoDirectories = getDirectories(dir=path.join(path.dirname(path.realpath(__file__)), "videos"))

	def processFiles(frames=[],videoArchive="",audioArchive="",frameRateFile=""):
		if path.isfile(videoArchive) and path.isfile(audioArchive) and path.isfile(frameRateFile):
				# prepare data
				if len(frames) == 0:# check if frames are yet loaded
					tar = tarfile.open(videoArchive)
					xpos = 0
					for i, member in enumerate(tar.getmembers()):
						f = tar.extractfile(member)
						frames.append(eval(f.read()))
						if i % 10 == 0:
							if xpos < 14:
								xpos += 1
							else:
								xpos = 0
							u.u.set_pixel(7,xpos-1,0,0,0)
							u.u.set_pixel(7,xpos,255,0,0)
							u.show()

						if nextDirButton:
							input_state = GPIO.input(nextDirButton)
							if input_state == 0: # get next directory
								u.clear()
								u.show()
								return frames

						if exitButton:
							exit_state = GPIO.input(exitButton)
							if exit_state == 0:
								u.clear()
								u.show()
								tar.close()
								exit()

					tar.close()

				with open(frameRateFile) as f: # 0.04 = 1 sec / 25 frames
					frameDuration = 1 / float(f.read())

				# start process
				elapsedTime = time.time() # start elapsed time
				playMusic(audioArchive)

				for frame in frames:
					u.putData(frame)
					u.show()
					elapsedTime += frameDuration

					timeLag = elapsedTime - time.time()
					if timeLag > 0.01: # control time frame in millisecond
						time.sleep(timeLag)

					if nextDirButton:
						input_state = GPIO.input(nextDirButton)
						if input_state == 0 : # get next directory
							u.clear()
							u.show()
							stopMusic()
							return frames

					if exitButton:
						exit_state = GPIO.input(exitButton)
						if exit_state == 0:
							u.clear()
							u.show()
							stopMusic()
							exit()

				stopMusic()
				# end process 
		return frames

	frames = []
	try:
		while True:
			if len(videoDirectories) == 0:exit()
			if len(videoDirectories) == 1: # one directory then loop contigous
				directory = videoDirectories[0]
				videoArchive = path.join(directory, "archive.tar")
				audioArchive = path.join(directory, "audio.mp3")
				frameRateFile = path.join(directory, "framerate.txt")
				u.clear()
				u.show()

				frames = processFiles(frames=frames,videoArchive=videoArchive,audioArchive=audioArchive,frameRateFile=frameRateFile)

			elif len(videoDirectories) > 1:
				# loop
				if d == len(videoDirectories): d = 0

				directory = videoDirectories[d]
				videoArchive = path.join(directory, "archive.tar")
				audioArchive = path.join(directory, "audio.mp3")
				frameRateFile = path.join(directory, "framerate.txt")
				u.clear()
				u.show()

				processFiles(frames=[],videoArchive=videoArchive,audioArchive=audioArchive,frameRateFile=frameRateFile)

				d += 1 # go next directory
	except KeyboardInterrupt:
		u.clear()
		u.off()
		try:
			stopMusic()
			print(" stop while")
		except:
			print(" stop while")
