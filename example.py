#!/usr/bin/env python
from __future__ import print_function
from os import listdir, path
from time import sleep
import tarfile

from unihat import UniHat

if __name__ == "__main__":
	u = UniHat()
	u.clear()
	archive = "./videos/alice/archive.tar"

	u.scrollText(text="Alice", xy=(0,4), color="red", speed=20)
	
	# load data
	data = []
	try:
		tar = tarfile.open(archive)
		for i, member in enumerate(tar.getmembers()):
			f = tar.extractfile(member)
			data.append(eval(f.read()))
			
			if i % 10 == 0:
				if xpos < 14:
					xpos += 1
				else:
					xpos = 0
					
				u.u.set_pixel(7,xpos-1,0,0,0)
				u.u.set_pixel(7,xpos,255,0,0)
				u.show()	
	except:
		u.clear()
		u.scrollText(text="No files!", xy=(0,4), color="red", speed=20)
		u.off()
		exit()

	# show data
	try:
		while True:
			for dat in data:
				u.putData(dat)
				u.show()
				sleep(0.01)
				
			u.clear()
			sleep(0.5)
			u.scrollText(text="Alice", xy=(0,4), color="blue")
			sleep(0.2)
	except KeyboardInterrupt:
		u.clear()
		u.off()
